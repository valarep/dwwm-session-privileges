<?php
use DWWM\Controller\SessionManager;
use DWWM\Model\Classes\Attribution;
?>
<?php require "html-head.html.php"; ?>
    <body>
<?php require "login.html.php"; ?>
<?php require "nav.html.php"; ?>
        <h1>DWWM - Session</h1>
        <h2>Group Management</h2>
<?php if($this->isConnected): ?>        
<?php if (count(SessionManager::hasPrivileges("attribution/read", true)) == 1): ?>
        <form method="post">
            <table>
                <thead>
                    <tr>
                        <th></th>
<?php foreach($this->view_privileges as $privilege): ?>
                        <th><?= $privilege->nom ?></th>
<?php endforeach; ?>        
                    </tr>
                </thead>
                <tbody>
<?php foreach($this->view_groupes as $groupe): ?>
                    <tr>
                        <th><?= $groupe->nom ?></th>
<?php foreach($this->view_privileges as $privilege): ?>
<?php
$attr = new Attribution($groupe->id, $privilege->id);
$json = json_encode($attr);
$checked = in_array($json, $this->view_attributions, true);
?>
                        <td style="text-align:center;"><input type="checkbox" name="attributions[]" value="<?= "{$groupe->id}_{$privilege->id}"; ?>" <?= ($checked)?'checked="checked"':''; ?>></td>
<?php endforeach; ?>        
                    </tr>
<?php endforeach; ?>        
                </tbody>
            </table>
            <input type="submit" name="btnSubmit" value="Mettre-à-jour" formaction="?action=Attribution-SubmitUpdate">
        </form>
<?php endif; ?>        
<?php endif; ?>        
    </body>
</html>