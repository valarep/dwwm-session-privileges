<?php
use DWWM\Controller\SessionManager;
?>
<?php require "html-head.html.php"; ?>
    <body>
<?php require "login.html.php"; ?>
<?php require "nav.html.php"; ?>
        <h1>DWWM - Session</h1>
        <h2>Edit User</h2>
<?php if($this->isConnected): ?>        
<?php if (count(SessionManager::hasPrivileges("utilisateur/update", true)) == 1): ?>
        <form method="post">
            <div>
                <input type="hidden" name="id" value="<?= $this->edited_user->id; ?>"><br>
                login <input type="text" name="login" value="<?= $this->edited_user->login; ?>"><br>
                <input type="submit" formaction="?action=User-Update-Submit" name="btn-update" value="Mettre-à-jour">
            </div>
            <div style="display:table;">
            <div style="display:table-cell;">
            groupes affectés<br>
            <select name="affected_groupes" size="10" style="width:150px;">
<?php foreach($this->groupes_affectes as $groupe): ?>        
                <option value="<?= $groupe->id; ?>"><?= $groupe->nom; ?></option>
<?php endforeach; ?>
            </select>
            </div>
            <div style="display:table-cell; padding:10px; vertical-align:middle;">
                <p><button type="submit" formaction="?action=Group-Affect"> <= </button></p>
                <p><button type="submit" formaction="?action=Group-Disaffect"> => </button></p>
            </div>
            <div style="display:table-cell;">
            groupes non-affectés<br>
            <select name="not_affected_groupes" size="10" style="width:150px;">
<?php foreach($this->groupes_non_affectes as $groupe): ?>        
                <option value="<?= $groupe->id; ?>"><?= $groupe->nom; ?></option>
<?php endforeach; ?>
            </select>
            </div>
            </div>
        </form>
<?php endif; ?>        
<?php endif; ?>        
    </body>
</html>