<?php
use DWWM\Controller\SessionManager;
?>
        <nav class="nav administrateurs">
            <span class="nav title">Administrateur(s)</span>
<?php if (count(SessionManager::hasPrivileges("utilisateur", false)) > 0): ?>
            <a href="?action=User-List">Utilisateurs</a>
<?php endif; ?>
<?php if (count(SessionManager::hasPrivileges("utilisateur", false)) > 0 && count(SessionManager::hasPrivileges("groupe", false)) > 0): ?>
             |
<?php endif; ?>
<?php if (count(SessionManager::hasPrivileges("attribution", false)) > 0): ?>
            <a href="?action=Attribution-Update">Groupes</a>
<?php endif; ?>
        </nav>
