<?php 
    if(in_array("Administrateur(s)", $this->groupes))
    {
        require "nav-administrateurs.html.php";
    }
    if(in_array("Comptable(s)", $this->groupes))
    {
        require "nav-comptables.html.php";
    }
    if(in_array("Directeur(s)", $this->groupes))
    {
        require "nav-directeurs.html.php";
    }
    if(in_array("Technicien(s)", $this->groupes))
    {
        require "nav-techniciens.html.php";
    }
    if(in_array("Secrétaire(s)", $this->groupes))
    {
        require "nav-secretaires.html.php";
    }
?>
