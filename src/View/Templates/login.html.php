<?php if(! $this->isConnected): ?>
<?php if(isset($this->message)): ?>
        <?= $this->message; ?>
<?php endif; ?>
        <form style="display:inline;" action="?action=SubmitLogin" method="post">
            <input type="text" name="login" placeholder="login">
            <input type="password" name="password" placeholder="password">
            <input type="submit" name="btn_submit" value="Connection">
        </form>
<?php else: ?>
        <?= $this->user->login; ?>
        <form style="display:inline;" action="?action=Disconnect" method="post">
            <input type="submit" name="btnDisconnect" value="Disconnect">
        </form>
<?php endif; ?>
