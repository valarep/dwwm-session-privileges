<?php
namespace DWWM\View;

class View
{
    public $template;

    public function __construct($template)
    {
        $this->template = $template;
    }

    public function bindParam($parameter, $value)
    {
        $this->$parameter = $value;
    }

    public function display()
    {
        include "Templates/" . $this->template . ".html.php";
    }
}