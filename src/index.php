<?php
namespace DWWM;
use DWWM\Controller\AttributionController;
use DWWM\Controller\UtilisateurController;
use DWWM\Controller\SessionManager;

require "../vendor/autoload.php";
SessionManager::start();

$action = filter_input(INPUT_GET, "action", FILTER_SANITIZE_STRING);
if (empty($action))
{
    // on n'a pas d'action => action par défaut
    if (SessionManager::isConnected())
    {
        // on est connecté => page welcome
        $action = "Welcome";
    }
    else
    {
        // on n'est pas connecté => page login
        $action = "Login";
    }
}

switch ($action)
{
    case "Login":
        UtilisateurController::loginAction();
        break;
    case "SubmitLogin":
        $login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
        $password = md5($password);

        UtilisateurController::submitLoginAction($login, $password);
        break;
    case "Welcome":
        UtilisateurController::welcomeAction();
        break;
    case "User-List":
        UtilisateurController::listAction();
        break;
    case "User-Update":
        $id = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT);
        UtilisateurController::updateAction($id);
        break;
    case "Group-Affect":
        $id_utilisateur = filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);
        $id_groupe = filter_input(INPUT_POST, "not_affected_groupes", FILTER_SANITIZE_NUMBER_INT);
        UtilisateurController::affectGroupeAction($id_utilisateur, $id_groupe);
        break;
    case "Group-Disaffect":
        $id_utilisateur = filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT);
        $id_groupe = filter_input(INPUT_POST, "affected_groupes", FILTER_SANITIZE_NUMBER_INT);
        UtilisateurController::disaffectGroupeAction($id_utilisateur, $id_groupe);
        break;
    case "Attribution-Update":
        AttributionController::updateAction();
        break;
    case "Attribution-SubmitUpdate":
        unset($_POST['btnSubmit']);
        $keys = array_keys($_POST);
        $values = [];
        $options = ["options"=>["regexp"=>"/^[0-9]+_[0-9]+$/"]];

        foreach($_POST['attributions'] as $value)
        {
            $filtered = filter_var($value, FILTER_VALIDATE_REGEXP, $options);
            if (!empty($filtered))
            {
                $array = explode("_", $filtered);
                $id_groupe = $array[0];
                $id_privilege = $array[1];
                $values[] = 
                [
                    "id_groupe" => $id_groupe, 
                    "id_privilege" => $id_privilege
                ];
            }
        }
        AttributionController::submitUpdateAction($values);
        break;
    case "Disconnect":
        UtilisateurController::disconnectAction();
        break;
    case "404":
        UtilisateurController::_404Action();
        break;
    default:
        UtilisateurController::_404Action();
        break;
}
