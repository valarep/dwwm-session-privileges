<?php
namespace DWWM\Controller;

use DWWM\Model\Classes\Affectation;
use DWWM\Model\Classes\Groupe;
use DWWM\Model\Classes\Utilisateur;
use DWWM\View\View;

class UtilisateurController
{
    public static function loginAction()
    {
        $isConnected = SessionManager::isConnected();
        $groupes = [];
        $privileges = [];

        $view = new View("welcome");
        $view->bindParam("groupes", $groupes);
        $view->bindParam("privileges", $privileges);
        $view->bindParam("isConnected", $isConnected);
        $view->display();
    }

    public static function submitLoginAction($login, $password)
    {
        $user = new Utilisateur(null, $login, $password);
        SessionManager::connect($user);
        $isConnected = SessionManager::isConnected();
        if ($isConnected)
        {
            // utilisateur valide
            // redirection vers la page d'accueil
            $origin = Router::getOrigin();
            $path = Router::getPath();
            header("location: {$origin}{$path}");
        }
        else
        {
            // utilisateur invalide
            $message = "Login or password error !";
            $groupes = [];
            $privileges = [];
            $view = new View("welcome");
            $view->bindParam("message", $message);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->display();
        }
    }

    public static function welcomeAction()
    {
        $user = SessionManager::getUser();
        $groupes = SessionManager::getGroupes();
        $privileges = SessionManager::getPrivileges();

        $temp_groupes = [];
        foreach ($groupes as $groupe)
        {
            $temp_groupes[] = $groupe->nom;
        }

        $temp_privileges = [];
        foreach ($privileges as $privilege)
        {
            $temp_privileges[] = $privilege->nom;
        }
        
        $isConnected = SessionManager::isConnected();

        $view = new View("welcome");
        $view->bindParam("user", $user);
        $view->bindParam("groupes", $temp_groupes);
        $view->bindParam("privileges", $temp_privileges);
        $view->bindParam("isConnected", $isConnected);
        $view->display();
    }

    public static function disconnectAction()
    {
        SessionManager::disconnect();
        // redirection vers la page d'accueil
        $origin = Router::getOrigin();
        $path = Router::getPath();
        header("location: {$origin}{$path}");
    }

    public static function listAction()
    {
        if (count(SessionManager::hasPrivileges("utilisateur/read", true)) == 1)
        {
            $user = SessionManager::getUser();
            $groupes = SessionManager::getGroupes();
            $privileges = SessionManager::getPrivileges();
            $isConnected = SessionManager::isConnected();

            $users = Utilisateur::getAll();
    
            $view = new View("user_list");
            $view->bindParam("user", $user);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->bindParam("users", $users);
            $view->display();
        }
        else
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();
            $options = "action=404";
            header("location: {$origin}{$path}?{$options}");
        }
    }

    public static function updateAction($id)
    {
        if (count(SessionManager::hasPrivileges("utilisateur/update", true)) == 1)
        {
            $user = SessionManager::getUser();
            $groupes = SessionManager::getGroupes();
            $privileges = SessionManager::getPrivileges();
            $isConnected = SessionManager::isConnected();

            $edited_user = Utilisateur::get($id);
            $groupes_affectes = Groupe::getGroupesByUtilisateur($id);
            $groupes_non_affectes = Groupe::getNotAffectedGroupesByUtilisateur($id);
            
            $view = new View("user_update");
            $view->bindParam("user", $user);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->bindParam("edited_user", $edited_user);
            $view->bindParam("groupes_affectes", $groupes_affectes);
            $view->bindParam("groupes_non_affectes", $groupes_non_affectes);
            $view->display();
        }
        else
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();
            $options = "action=404";
            header("location: {$origin}{$path}?{$options}");
        }
    }
    public static function _404Action()
    {
        $user = SessionManager::getUser();
        $groupes = SessionManager::getGroupes();
        $privileges = SessionManager::getPrivileges();
        $isConnected = SessionManager::isConnected();

        $view = new View("404");
        $view->bindParam("user", $user);
        $view->bindParam("groupes", $groupes);
        $view->bindParam("privileges", $privileges);
        $view->bindParam("isConnected", $isConnected);
        $view->display();
    }

    public static function affectGroupeAction($id_utilisateur, $id_groupe)
    {
        if(! empty($id_groupe))
        {
            Affectation::affect($id_utilisateur, $id_groupe);
        }

        // retour sur la page User Update
        $origin = Router::getOrigin();
        $path = Router::getPath();
        $options = "action=User-Update&id={$id_utilisateur}";
        header("location: {$origin}{$path}?{$options}");
    }

    public static function disaffectGroupeAction($id_utilisateur, $id_groupe)
    {
        if(! empty($id_groupe))
        {
            Affectation::disaffect($id_utilisateur, $id_groupe);
        }

        // retour sur la page User Update
        $origin = Router::getOrigin();
        $path = Router::getPath();
        $options = "action=User-Update&id={$id_utilisateur}";
        header("location: {$origin}{$path}?{$options}");
    }
}