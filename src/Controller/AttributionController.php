<?php
namespace DWWM\Controller;

use DWWM\Model\Classes\Attribution;
use DWWM\Model\Classes\Groupe;
use DWWM\Model\Classes\Privilege;
use DWWM\View\View;

class AttributionController
{
    public static function updateAction()
    {
        if (count(SessionManager::hasPrivileges("attribution/read", true)) == 1)
        {
            $user = SessionManager::getUser();
            $groupes = SessionManager::getGroupes();
            $privileges = SessionManager::getPrivileges();
            $isConnected = SessionManager::isConnected();

            $view_privileges = Privilege::getAll();
            $view_groupes = Groupe::getAll();
            $temp_attributions = Attribution::getAll();
            $view_attributions = [];
            foreach($temp_attributions as $attr)
            {
                $view_attributions[] = json_encode($attr);
            }
            
            $view = new View("attribution_update");
            $view->bindParam("user", $user);
            $view->bindParam("groupes", $groupes);
            $view->bindParam("privileges", $privileges);
            $view->bindParam("isConnected", $isConnected);
            $view->bindParam("view_privileges", $view_privileges);
            $view->bindParam("view_groupes", $view_groupes);
            $view->bindParam("view_attributions", $view_attributions);
            $view->display();
        }
        else
        {
            $origin = Router::getOrigin();
            $path = Router::getPath();
            $options = "action=404";
            header("location: {$origin}{$path}?{$options}");
        }
    }
    public static function submitUpdateAction($values)
    {
        Attribution::update($values);
        
        $origin = Router::getOrigin();
        $path = Router::getPath();
        $options = "action=Attribution-Update";
        header("location: {$origin}{$path}?{$options}");
    }
}