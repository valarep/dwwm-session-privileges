-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 26 avr. 2019 à 07:17
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dwwm_session`
--

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`) VALUES
(1, 'Administrateur(s)'),
(2, 'Comptable(s)'),
(3, 'Technicien(s)'),
(4, 'Secrétaire(s)'),
(5, 'Directeur(s)');

-- --------------------------------------------------------

--
-- Structure de la table `groupe_privilege`
--

DROP TABLE IF EXISTS `groupe_privilege`;
CREATE TABLE IF NOT EXISTS `groupe_privilege` (
  `id_groupe` int(11) NOT NULL,
  `id_privilege` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_privilege`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe_privilege`
--

INSERT INTO `groupe_privilege` (`id_groupe`, `id_privilege`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(2, 2),
(2, 6),
(2, 9),
(2, 11),
(2, 14),
(3, 2),
(3, 6),
(3, 9),
(3, 11),
(3, 14),
(4, 2),
(4, 6),
(4, 9),
(4, 11),
(4, 14),
(5, 1),
(5, 2),
(5, 5),
(5, 6),
(5, 9),
(5, 11),
(5, 14);

-- --------------------------------------------------------

--
-- Structure de la table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
CREATE TABLE IF NOT EXISTS `privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `privilege`
--

INSERT INTO `privilege` (`id`, `nom`) VALUES
(1, 'utilisateur/create'),
(2, 'utilisateur/read'),
(3, 'utilisateur/update'),
(4, 'utilisateur/delete'),
(5, 'groupe/create'),
(6, 'groupe/read'),
(7, 'groupe/update'),
(8, 'groupe/delete'),
(9, 'privilege/read'),
(10, 'affectation/create'),
(11, 'affectation/read'),
(12, 'affectation/delete'),
(13, 'attribution/create'),
(14, 'attribution/read'),
(15, 'attribution/delete');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `password`) VALUES
(1, 'test', '098f6bcd4621d373cade4e832627b4f6'),
(2, 'david.riehl', '81dc9bdb52d04dc20036dbd8313ed055'),
(3, 'bernard.dupont', '674f3c2c1a8a6f90461e8a66fb5550ba'),
(4, 'michael.cartier', '0acf4539a14b3aa27deeb4cbdf6e989f'),
(5, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_groupe`
--

DROP TABLE IF EXISTS `utilisateur_groupe`;
CREATE TABLE IF NOT EXISTS `utilisateur_groupe` (
  `id_utilisateur` int(11) NOT NULL,
  `id_groupe` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`,`id_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur_groupe`
--

INSERT INTO `utilisateur_groupe` (`id_utilisateur`, `id_groupe`) VALUES
(2, 3),
(2, 5),
(4, 3),
(5, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
